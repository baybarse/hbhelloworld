FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim as build
WORKDIR /src
COPY ["/aspnetapp.csproj", "./"]
RUN dotnet restore "aspnetapp.csproj"
COPY . .
WORKDIR "/src"
RUN dotnet build "aspnetapp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "aspnetapp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "aspnetapp.dll"]




#FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
#WORKDIR /source

#COPY aspnetapp/*.csproj ./aspnetapp/
#RUN dotnet restore

#COPY aspnetapp/. ./aspnetapp/
#WORKDIR /source/aspnetapp
#RUN dotnet publish -c release -o /app --no-restore

#FROM mcr.microsoft.com/dotnet/aspnet:5.0
#WORKDIR /app
#COPY --from=build /app ./
#ENTRYPOINT ["dotnet", "aspnetapp.dll"]
